import Vue from 'vue'
import Router from 'vue-router'
import VueX from '@/views/vuex/index'
import State from '@/views/state/index'
import Mutation from '@/views/mutation/index'
import Getter from '@/views/getter/index'
import Action from '@/views/action/index'
import Module from '@/views/modules/index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/vuex',
      name: 'vuex',
      component: VueX
    },
    {
      path: '/state',
      name: 'state',
      component: State
    },
    {
      path: '/mutation',
      name: 'mutation',
      component: Mutation
    },
    {
      path: '/getter',
      name: 'getter',
      component: Getter
    },
    {
      path: '/action',
      name: 'action',
      component: Action
    },
    {
      path: '/module',
      name: 'module',
      component: Module
    }
  ]
})
