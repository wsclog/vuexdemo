const actions = {
  addNumAction (context, num) {
    context.commit('addNum', num)
  },
  addCountAction (context) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        context.commit('add')
        resolve()
      }, 1000)
    })
  },
  reduceCountAction (context) {
    context.state.count--
  },
  addTwoAction (context) {
    context.dispatch('addCountAction').then(() => {
      context.commit('addTwo')
    })
  }
}
export default actions
