import { ADD } from './mutation-types'
const mutations = {
  addNum (state) {
    state.num++
  },
  [ADD] (state) {
    state.count++
  },
  addTwo (state) {
    state.count += 2
  }
}

export default mutations
