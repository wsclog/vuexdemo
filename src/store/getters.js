const getters = {
  getNum (state) {
    return state.num
  },
  getCount (state) {
    return state.count
  }
}

export default getters
